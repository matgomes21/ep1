# EP1 OO 2019.2 (UnB - Gama)

### Aluno: Mateus Gomes do Nascimento
### Matrícula: 18/0106821
### Turma B (Professora Carla)

## Lista de dependências:

Neste projeto foram utilizadas as seguintes bibliotecas:

 - iostream
 - string
 - vector
 - stdbool.h
 - fstream
 - cstdlib

## Menu Inicial:

O primeiro menu contém quatro opções:

  1. Modo venda: Acessa o modo venda;
  2. Modo recomendação: Acessa o modo recomendação;
  3. Modo estoque: Acessa o modo estoque;
  4. Sair (digitando a tecla 'v').

## Modo Venda:

Ao entrar no modo venda, é solicitado o cpf do cliente. Caso o cpf não esteja cadastrado no arquivo "socios.txt" (na pasta archives) é feito o cadastro de novo sócio e depois é acessado o menu do modo venda, e caso o cpf já esteja cadastrado é acessado direto o menu do modo venda.

### Menu venda:

Este menu possui as seguintes opções:

  1. Mostrar estoque: Imprime na tela os produtos que estão armazenados no estoque.

  2. Adicionar produto ao carrinho: Nesta opção é possível adicionar novos produtos no carrinho, quando se acessa esta opção é solicitado o nome do produto que deseja ser adicionado, e caso este produto esteja cadastrado no estoque, o mesmo é adicionado no carrinho.

  3. Remover produto do carrinho: Nesta opção é possível remover um produto do carrinho, quando se acessa esta opção é solicitado o nome do produto, e caso o mesmo esteja no carrinho, é removida 1 quantidade do produto.

  4. Mostrar carrinho: Imprime na tela os produtos que estão inseridos no carrinho.

  5. Finalizar compra: Quando se acessa esta opção é mostrado na tela os produtos, juntamente com seus atributos, que estão no carrinho, o preço total da compra, o desconto para sócio e o preço com desconto, e juntamente com estes dados é apresentado duas opções, uma para confirmar compra e a outra para cancelar e voltar ao menu de venda.

  6. Voltar ao menu anterior: Quanto digita-se a tecla 'v', volta-se para o menu principal.

## Modo estoque:

O menu do modo estoque possui as seguintes opções:

  1. Mostrar estoque: Imprime na tela os produtos que estão armazenados no estoque.

  2. Adicionar produto: Adiciona novos produtos ao estoque, nesta opção é feito um cadastro do produto solicitando seus atributos.

  3. Mostrar categorias: Imprime na tela as categorias de produtos existentes.

  4. Adicionar categoria: Nesta opção é possível adicionar uma nova categoria que pode ser atribuida aos produtos.

  5. Mudar quantidade de produto estocado: Nesta opção é possível atualizar a quantidade de algum produto do estoque.

  6. Ao digitar 'v', volta-se ao menu inicial.

## Modo recomendação:

Ao entrar neste modo, é solicitado o cpf do cliente para verificar se o mesmo é sócio. E a partir do histórico de compras do sócio é recomendados produtos para o mesmo.
