//Includdes de hpp necessarios
#include "carrinho.hpp"
#include "produto.hpp"

using namespace std;

//==== Construtor ====
Carrinho::Carrinho(){
    set_preco_total();
    set_carrinho_len();
    set_estoque_len();
    load_estoque();
}
//==== Destrutor ====
Carrinho::~Carrinho(){
}
//==== Metodos Acessores Setters ====
//Determina o tamanho do carrinho
void Carrinho::set_carrinho_len(){
    this->carrinho_len = this->carrinho.size();
}
void Carrinho::set_preco_total(){
    if(this->carrinho_len!=0){
      this->preco_total = 0;
      for(int i=0;i<this->carrinho_len;i++){
        this->preco_total+=this->carrinho[i]->get_preco()*this->carrinho[i]->get_quantidade();
      }
    }
    else
      this->preco_total=0;
}
//==== Metodos Acessores Getters ====
//Recebe tamanho do carrinho
int Carrinho::get_carrinho_len(){
    return carrinho_len;
}
//Recebe preco total
double Carrinho::get_preco_total(){
    return preco_total;
}
//Recebe carrinho em vector
vector <Produto*> Carrinho::get_carrinho(){
  return carrinho;
}
//==== Outros Metodos ====
//Imprime produtos do carrinho
void Carrinho::imprime_carrinho(){
    for(int i=0;i<this->carrinho_len;i++){
      cout << "Nome do produto: " << this->carrinho[i]->get_nome_produto() << endl;
      cout << "Quantidade: " << this->carrinho[i]->get_quantidade() << endl;
      cout << "Categorias: " << this->carrinho[i]->get_categorias_len() << endl;
      for(int j=0;j<this->carrinho[i]->get_categorias_len();j++){
        cout << this->carrinho[i]->get_categorias(j) << "/";
      }
      cout << endl << "===============================================" << endl;
    }
}
//Adiciona produto ao carrinho
void Carrinho::add_produto(){
    string nome_produto;
    cout << "Digite o nome do produto: ";
    getline(cin,nome_produto);
    for(int i=0;i<this->carrinho_len;i++){
      if(nome_produto==this->carrinho[i]->get_nome_produto()){
        this->carrinho[i]->set_quantidade(this->carrinho[i]->get_quantidade()+1);
        cout << endl << "Produto adicionao ao carrinho!" << endl;
        return;
      }
    }
    Produto *new_produto;
    new_produto = search_produto(nome_produto);
    if(new_produto==NULL)
      return;
    new_produto->set_quantidade(1);
    if(new_produto!=NULL){
      this->carrinho.push_back(new_produto);
      this->carrinho_len++;
      cout << endl << "Produto adicionado ao carrinho!" << endl;
    }
}
//Remove produto do carrinho
void Carrinho::remove_produto(){
  string nome_produto;
  cout << "Digite o nome do produto: ";
  getline(cin,nome_produto);
  for(int i=0;i<this->carrinho_len;i++){
    if(nome_produto==this->carrinho[i]->get_nome_produto()){
      this->carrinho.erase(this->carrinho.begin()+i);
      this->carrinho_len--;
      cout << endl<< "Produto removido do carrinho!" << endl;
      return;
    }
  }
  cout << "Produto nao encontrado no carrinho." << endl;
}
// Finaliza a compra
bool Carrinho::finalizar_compra(){
  int escolha;
  set_preco_total();
  system("clear");
  cout << "Produtos no carrinho:" << endl << endl;
  for(int i=0;i<this->carrinho_len;i++){
    cout << this->carrinho[i]->get_nome_produto() << " | R$ " << this->carrinho[i]->get_preco() << " | Quantidade: " << this->carrinho[i]->get_quantidade() << endl;
  }
  cout << endl << "Preço total: R$ " << get_preco_total() << endl;
  cout << "Valor do desconto para socio: R$ " << get_preco_total() * 0.15 << endl;
  cout << "Preço final com desconto: R$ " << get_preco_total() - (get_preco_total()*0.15) << endl;
  cout << endl << "(1) Confirmar" << endl << "(0) Cancelar" << endl;
  cin >> escolha;
  if(escolha==1){
    Produto *new_produto;
    for(int i=0;i<this->carrinho_len;i++){
      new_produto = search_produto(this->carrinho[i]->get_nome_produto());
      if(new_produto->get_quantidade()<this->carrinho[i]->get_quantidade()){
        cout << endl << "[ERRO] Quantidade de compra maior que a quantidade no estoque!" << endl;
        cout << endl << "Aperte enter para prosseguir..." << endl;
        cin.ignore();
        getchar();
        return true;
      }
    }
    for(int i=0;i<this->carrinho_len;i++){
      mudar_quantidade(this->carrinho[i]->get_nome_produto(),this->carrinho[i]->get_quantidade());
    }
    cout << endl << "Compra finalizada!" << endl;
    cout << endl << "Aperte enter para prosseguir..." << endl;
    getchar();
    cin.ignore();
    return true;
  }
  else
    return false;
}
