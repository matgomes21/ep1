//includes das classes
#include "cliente.hpp"
#include "produto.hpp"
#include "carrinho.hpp"
#include "socio.hpp"
#include "estoque.hpp"
#include "recomendacao.hpp"
//includes das bibliotecas necessarias
#include <iostream>
#include <string>
#include <vector>
#include <fstream>

using namespace std;

int main(){

    system("clear");
    //Variaveis
    int decisao_final;
    char menu1,menu2;
    string nome,cpf,email,telefone,sexo;
    bool fim=0;
    //Vector para socio
    Socio* new_socio = new Socio();
    do{
    decisao_final=0; //Variavel util na opcao "Finalizar venda"
    //==== Menu inicial ====
    cout << "===== BEM-VINDO(A) AO TERMINAL DA LOJA VICTORIA GAMES! =====" << endl;
    cout << "============================================================" << endl;
    cout << "Escolha o que deseja fazer:" << endl;
    cout << "(1) Modo venda" << endl;
    cout << "(2) Modo recomendação" << endl;
    cout << "(3) Modo estoque" << endl;
    cout << "Digite 's' para sair" << endl;

    do{ //Do while para validar a entrada
      cin >> menu1; //Entrada da escolha
      cin.ignore();
      if((menu1<49||menu1>51)&&menu1!='s'){
        cout << endl << "Opção inválida, por favor digite a opção novamente: ";
      }
    }while((menu1<49||menu1>51)&&menu1!='s');

    system("clear");
    //==== Opcoes ====
    switch(menu1){
      //==== Modo venda ====
      case '1':{
        new_socio->add_socio();
        getchar();
        Carrinho* new_carrinho = new Carrinho();
        do{
          system("clear");
          //==== Menu venda ====
          cout << "===== MODO VENDA =====" << endl;
          cout << "Escolha o que deseja fazer:" << endl;
          cout << "(1) Mostrar estoque" << endl;
          cout << "(2) Adicionar produto ao carrinho" << endl;
          cout << "(3) Remover produto do carrinho" << endl;
          cout << "(4) Mostrar carrinho" << endl;
          cout << "(5) Finalizar compra" << endl;
          cout << "Digite 'v' para voltar ao menu anterior" << endl;

          do{ //Do while para validar a entrada
            cin >> menu2; //Entrada da escolha
            cin.ignore();
            if((menu2<49||menu2>53)&&menu2!='v'){
              cout << endl << "Opção inválida, por favor digite a opção novamente: ";
            }
          }while((menu2<49||menu2>53)&&menu2!='v');

          system("clear");
          switch(menu2){
              case '1':{
                new_carrinho->imprime_estoque();
                cout << endl << "Aperte enter para prosseguir..." << endl;
                getchar();
                break;
              }
              case '2':{
                new_carrinho->add_produto();
                cout << endl << "Aperte enter para prosseguir..." << endl;
                getchar();
                break;
              }
              case '3':{
                new_carrinho->remove_produto();
                cout << endl << "Aperte enter para prosseguir..." << endl;
                getchar();
                break;
              }
              case '4':{
                new_carrinho->imprime_carrinho();
                cin.ignore();
                cout << endl << "Aperte enter para prosseguir..." << endl;
                getchar();
                break;
              }
              case '5':{
                fim = new_carrinho->finalizar_compra();
                system("clear");
                if(fim==false){
                  decisao_final=0;
                }
                else{
                  decisao_final=1;
                  //...
                }
                break;
              }
              case 'v':{
                break;
              }
          }
        }while((menu2>=49&&menu2<=53)&&decisao_final==0);
        //Salva arquivos usados
        new_carrinho->save_estoque();
        new_carrinho->save_categorias();
        break;
      }
      //==== MODO RECOMENDACAO ====
      case '2':{
        decisao_final = new_socio->check_cpf();
        getchar();
        if(decisao_final==true){
          Estoque* new_estoque = new Estoque();
          Recomendacao* new_recomend = new Recomendacao(new_socio->get_cpf());
          new_recomend->carrega_estoque(new_estoque->vector_estoque());
          new_recomend->recomendacao();
          getchar();
        }
        break;
      }
      //==== Modo Estoque ====
      case '3':{
        //==== Instanciamento da classe estoque ====
        Estoque *new_estoque = new Estoque();
        do{
          system("clear");
          //==== Menu Estoque ====
          cout << "===== MODO ESTOQUE =====" << endl;
          cout << "Escolha o que deseja fazer:" << endl;
          cout << "(1) Mostrar estoque" << endl;
          cout << "(2) Adicionar produto" << endl;
          cout << "(3) Mostrar categorias" << endl;
          cout << "(4) Adicionar categoria" << endl;
          cout << "(5) Mudar quantidade de produto estocado" << endl;
          cout << "Digite 'v' para voltar ao menu principal." << endl;

          //Do while para validar a entrada
          do{
            cin >> menu2; //Opcao do menu estoque
            if((menu2<49||menu2>53)&&menu2!='v'){
              cout << endl << "Opção inválida, por favor digite a opção novamente: ";
            }
          }while((menu2<49||menu2>53)&&menu2!='v');

          system("clear");

          switch(menu2){
            //==== Imprime o estoque ====
            case '1':{
              new_estoque->imprime_estoque();
              cin.ignore();
              cout << endl << "Aperte enter para proseguir..." << endl;
              getchar();
              break;
            }
            //==== Adiciona produto ao estoque ====
            case '2':{
              new_estoque->add_produto();
              cout << endl << "Aperte enter para prosseguir..." << endl;
              cin.ignore();
              getchar();
              break;
            }
            //==== Imprime categorias existentes ====
            case '3':{
              new_estoque->get_list_categorias();
              cin.ignore();
              cout << endl << "Aperte enter para prosseguir..." << endl;
              getchar();
              break;
            }
            //==== Adiciona categoria ====
            case '4':{
              new_estoque->add_categoria();
              cout << endl << "Aperte enter para prosseguir..." << endl;
              getchar();
              break;
            }
            //==== Altera quantidade de produto estocado ====
            case '5':{
              new_estoque->mudar_quantidade();
              cout << endl << "Aperte enter para prosseguir..." << endl;
              getchar();
              break;
            }
            case 'v':{
              break;
            }
          }
        }while(menu2!='v');
        //Salva arquivos usados
        new_estoque->save_estoque();
        new_estoque->save_categorias();
        break;
      }
      case 's':{
        //Salva arquivo socio
        new_socio->save_socio();
        break;
      }
    }
  }while(menu1!='s');

    return 0;
}
