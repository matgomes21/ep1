#include "recomendacao.hpp" //Include do hpp
//Includes de mais bibliotecas necessarias
#include <fstream>
#include <cstdlib>

using namespace std;

//==== Construtores e Destrutor
Recomendacao::Recomendacao(){
}
Recomendacao::Recomendacao(string cpf){
  concat(cpf);
  load_recomendacao();
  set_historico_len();
  set_rcategorias_len();
}
Recomendacao::Recomendacao(vector <Produto*> historico){
  load_recomendacao();
  load_historico(historico);
  set_historico_len();
  set_rcategorias_len();
}
Recomendacao::~Recomendacao(){
}
//==== Setters ====
void Recomendacao::set_rcategorias_len(){
  this->rcategorias_len = int(this->categorias.size());
}
void Recomendacao::set_historico_len(){
  this->historico_len = int(this->historico.size());
}
//
void Recomendacao::concat(string cpf){
  string arquivo;
  string extensao = ".txt";
  arquivo = cpf + extensao;
  this->nome = arquivo;
}
void Recomendacao::sort(){
  if(this->rcategorias_len==0){
    cout << endl << "Nao foi possivel geral recomendacoes." << endl;
  }
  else{
    for(int i=0;i<this->rcategorias_len;i++){
      for(int j=0;j<this->rcategorias_len;j++){
        if(this->socio[i]<this->socio[j]){
          int aux = this->socio[j];
          this->socio[j] = this->socio[i];
          this->socio[i] = aux;
          string aux2 = this->categorias[j];
          this->categorias[j] = this->categorias[i];
          this->categorias[i] = aux2;
        }
      }
    }
  }
}
void Recomendacao::save_recomendacao(){
  ofstream txt;
  txt.open("archives/"+this->nome);
  txt << this->rcategorias_len << endl;
  for(int i=0;i<this->rcategorias_len;i++){
    txt << this->categorias[i] << endl;
    txt << this->socio[i] << endl;
  }
  txt.close();
}
bool Recomendacao::check_recomendacao(){
  ifstream txt;
  txt.open("archives/"+this->nome);
  if(txt.is_open()){
    txt.close();
    return true;
  }
  else
    return false;
}
void Recomendacao::load_recomendacao(){
  if(check_recomendacao()==true){
    string categoria,len,qtd;
    ifstream txt;
    int len2,qtd2;
    txt.open("archives/"+this->nome);
    getline(txt,len);
    len2 = atoi(len.c_str());
    for(int i=0;i<len2;i++){
      vector <string> categorias;
      getline(txt,categoria);
      getline(txt,qtd);
      qtd2 = atoi(qtd.c_str());
      this->categorias.push_back(categoria);
      this->socio.push_back(qtd2);
    }
    txt.close();
  }
}
void Recomendacao::carrega_estoque(vector <Produto*> estoque){
  this->estoque = estoque;
}
void Recomendacao::load_historico(vector <Produto*> historico){
  this->historico = historico;
}
void Recomendacao::define_categorias(){
  for(int i=0;i<this->historico_len;i++){
    for(int j=0;j<this->historico[i]->get_categorias_len();j++){
      set_rcategorias_len();
      if(this->rcategorias_len==0){
        this->categorias.push_back(this->historico[i]->get_categorias(j));
        this->socio.push_back(1);
        this->rcategorias_len++;
      }
      else{
        int aux=0;
        for(int k=0;k<this->rcategorias_len;k++){
          if(this->categorias[k]==this->historico[i]->get_categorias(j)){
            this->socio[k]++;
            aux++;
          }
        }
        if(aux==0){
          this->categorias.push_back(this->historico[i]->get_categorias(j));
          this->socio.push_back(1);
          this->rcategorias_len++;
        }
      }
    }
  }
}
void Recomendacao::recomendacao(){
  sort();
  int c=0;
  cout << "Produtos recomendados:" << endl;
  for(int i=this->rcategorias_len-1;i>=0;i--){
    c++; //rsrs
    vector <string> recomend;
    for(int j=0;j<int(this->estoque.size());j++){
      for(int k=0;k<int(this->estoque[j]->get_categorias_len());k++){
        if(this->categorias[i]==this->estoque[j]->get_categorias(k)){
          recomend.push_back(this->estoque[i]->get_nome_produto());
        }
      }
    }
    if(c==1){
      if(int(recomend.size()<5)){
        for(int l=0;l<int(recomend.size());l++){
          int len = int(recomend.size());
          int random = rand()%len;
          cout << recomend[random] << endl;
          recomend.erase(recomend.begin()+random);
        }
      }
      else{
        for(int l=0;l<5;l++){
          int len = int(recomend.size());
          int random = rand()%len;
          cout << recomend[random] << endl;
          recomend.erase(recomend.begin()+random);
        }
      }
    }
    if(c==2){
        if(int(recomend.size()<3)){
            for(int l=0;l<int(recomend.size());l++){
                int len=int(recomend.size());
                int random=rand()%len;
                cout << recomend[random] << endl;
                recomend.erase(recomend.begin()+random);
            }
        }
        else{
            for(int l=0;l<3;l++){
                int len=int(recomend.size());
                int random=rand()%len;
                cout << recomend[random] << endl;
                recomend.erase(recomend.begin()+random);
            }
        }
    }
    if(c==3){
        if(int(recomend.size()<2)){
            for(int l=0;l<int(recomend.size());l++){
                int len=int(recomend.size());
                int random=rand()%len;
                cout << recomend[random] << endl;
                recomend.erase(recomend.begin()+random);
            }
        }
        else{
            for(int l=0;l<2;l++){
                int len=int(recomend.size());
                int random=rand()%len;
                cout << recomend[random] << endl;
                recomend.erase(recomend.begin()+random);
            }
          }
        }
      }
}
