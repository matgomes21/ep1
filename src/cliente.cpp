#include "cliente.hpp" //Include do hpp
#include <string> //Include de biblioteca necessaria

//Metodo construtor
Cliente::Cliente(){
  set_cpf("00000000000");
}
//Metodo destrutor
Cliente::~Cliente(){
}
//Metodo acessor (set)
void Cliente::set_cpf(string cpf){
  this->cpf = cpf;
}
//Metodo acessor (get)
string Cliente::get_cpf(){
  return cpf;
}
