#include "estoque.hpp" //Include do hpp
//Includes de bibliotecas necessarias
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <stdbool.h>

using namespace std;

//==== Metodo Construtor ====
Estoque::Estoque(){
    set_estoque_len();
    set_categoria_len();
    load_estoque();
    load_categorias();
}
//==== Metodo Destrutor ====
Estoque::~Estoque(){
}
//==== Acessores para o tamanho do estoque ====
//Set
void Estoque::set_estoque_len(){
  ifstream estoque;
  string estoque_len;
  estoque.open("archives/estoque.txt",ios::in);
  if(estoque.is_open()){
    getline(estoque,estoque_len);
    this->estoque_len=atoi(estoque_len.c_str());
    estoque.close();
  }
  else
    this->estoque_len = 0;
}
//Get
int Estoque::get_estoque_len(){
  return estoque_len;
}
//==== Metodo para buscar produto especifico ====
Produto* Estoque::search_produto(string nome_produto){
  for(int i=0;i<this->estoque_len;i++){
    if(nome_produto==this->estoque[i]->get_nome_produto()){
      Produto* new_produto = new Produto(this->estoque[i]->get_nome_produto(),this->estoque[i]->get_vcategorias(),this->estoque[i]->get_preco(),this->estoque[i]->get_quantidade(),this->estoque[i]->get_codigo());
      return new_produto;
    }
  }
  cout << endl << "Produto nao encontrado no estoque!" << endl;
  return NULL;
}
//==== Metodo para imprimir produtos do estoque ====
void Estoque::imprime_estoque(){
  for(int i=0; i<this->estoque_len;i++){
    cout << "Nome do produto: " << this->estoque[i]->get_nome_produto() << endl;
    cout << "Categoria(s): ";
    for(int j=0;j<this->estoque[i]->get_categorias_len();j++){
      cout << this->estoque[i]->get_categorias(j) << "/";
    }
    cout << endl << "Preço: R$ " << this->estoque[i]->get_preco() << endl;
    cout << "Quantidade: " << this->estoque[i]->get_quantidade() << endl;
    cout << "Codigo: " << this->estoque[i]->get_codigo() << endl;
    cout << endl;
  }
}
//==== Verifica se existe o arquivo do estoque ====
bool Estoque::check_estoque(){
  ifstream estoque;
  estoque.open("archives/estoque.txt",ios::in);
  if(estoque.is_open()){
    estoque.close();
    return true;
  }
  else
    return false;
}
//==== Carrega arquivo do estoque ====
void Estoque::load_estoque(){
  if(check_estoque()==true){
    string aux,nome_produto,categoria,preco,str_qtd,codigo,str_categoria_len;
    ifstream estoque;
    int int_qtd,int_categoria_len;
    double d_preco;
    estoque.open("archives/estoque.txt",ios::in);
    getline(estoque,aux);
    Produto* new_produto;
    for(int i=0;i<this->estoque_len;i++){
      vector <string> categorias;
      getline(estoque,nome_produto);
      getline(estoque,str_categoria_len);
      int_categoria_len = atoi(str_categoria_len.c_str());
      for(int j=0;j<int_categoria_len;j++){
        getline(estoque,categoria);
        categorias.push_back(categoria);
      }
      getline(estoque,preco);
      getline(estoque,str_qtd);
      int_qtd = atoi(str_qtd.c_str());
      getline(estoque,codigo);
      d_preco = atof(preco.c_str());
      new_produto = new Produto(nome_produto,categorias,d_preco,int_qtd,codigo);
      new_produto->set_categorias_len();
      this->estoque.push_back(new_produto);
    }
    estoque.close();
  }
}
//==== Adiciona novo produto ao estoque ====
void Estoque::add_produto(){
  vector <string> categorias;
  string nome_produto,categoria,codigo;
  int quantidade,categorias_len,c=0,i,j;
  double preco;

  cout << "Digite o nome do produto: ";
  cin.ignore();
  getline(cin,nome_produto);
  for(i=0;i<this->estoque_len;i++){
    if(nome_produto==this->estoque[i]->get_nome_produto()){
      cout << endl << "Produto ja presente no estoque." << endl;
      return;
    }
  }
  cout << endl << "Iniciando cadastro do produto..." << endl;
  cout << endl << "Informe quantas categorias o produto possui: ";
  cin >> categorias_len;
  cin.ignore();
  for(i=0;i<categorias_len;i++){
    cout << endl << "Digite a categoria" << i+1 << ": ";
    getline(cin,categoria);
    categorias.push_back(categoria);
  }
  cout << endl << "Informe o codigo do produto: ";
  getline(cin,codigo);
  cout << endl << "Preço do produto (em reais): ";
  cin >> preco;
  cout << endl << "Quantidade: ";
  cin.ignore();
  cin >> quantidade;
  for(i=0;i<this->categoria_len;i++){
    for(j=0;j<categorias_len;j++){
      if(categorias[j]==this->categoria[i])
        c++;
    }
  }
  if(c==categorias_len){
    Produto* new_produto = new Produto(nome_produto,categorias,preco,quantidade,codigo);
    new_produto->set_categorias_len();
    this->estoque.push_back(new_produto);
    this->estoque_len++;
    cout << endl << "Produto estocado!" << endl;
    return;
  }
  cout << "Categoria inexistente!" << endl;
}
//==== Salva arquivo do estoque ====
void Estoque::save_estoque(){
  if(check_estoque()==true||this->estoque.size()!=0){
    ofstream estoque;
    estoque.open("archives/estoque.txt",ios::out);
    estoque << this->estoque_len << endl;
    for(int i=0;i<this->estoque_len;i++){
      estoque << this->estoque[i]->get_nome_produto() << endl;
      estoque << this->estoque[i]->get_categorias_len() << endl;
      for(int j=0;j<this->estoque[i]->get_categorias_len();j++){
        estoque << this->estoque[i]->get_categorias(j) << endl;
      }
      estoque << this->estoque[i]->get_preco() << endl;
      estoque << this->estoque[i]->get_quantidade() << endl;
      estoque << this->estoque[i]->get_codigo() << endl;
    }
    estoque.close();
  }
}
//==== Determina a quantidade de categorias ====
void Estoque::set_categoria_len(){
  ifstream categorias;
  string categorias_len;
  categorias.open("archives/categorias.txt",ios::in);
  if(categorias.is_open()){
    getline(categorias,categorias_len);
    this->categoria_len = atoi(categorias_len.c_str());
    categorias.close();
  }
  else
    this->categoria_len = 0;
}
//==== Retorna numero de categorias ====
int Estoque::get_categoria_len(){
  return this->categoria_len;
}
//==== Verifica se existe o arquivo de categorias ====
bool Estoque::check_categorias(){
  ifstream categorias;
  categorias.open("archives/categorias.txt",ios::in);
  if(categorias.is_open()){
    categorias.close();
    return true;
  }
  else
    return false;
}
//==== Carrega arquivo de categorias ====
void Estoque::load_categorias(){
  if(check_categorias()==true){
    string categoria,aux;
    ifstream categorias;
    categorias.open("archives/categorias.txt",ios::in);
    getline(categorias,aux);
    for(int i=0;i<this->categoria_len;i++){
      getline(categorias,categoria);
      this->categoria.push_back(categoria);
    }
    categorias.close();
  }
}
//==== Pega a lista de categorias ====
void Estoque::get_list_categorias(){
  for(int i=0;i<this->categoria_len;i++){
    cout << this->categoria[i] << endl;
  }
}
//==== Adiciona categoria ====
void Estoque::add_categoria(){
  string categoria;
  cout << "Digite o nome da nova categoria: ";
  cin.ignore();
  getline(cin,categoria);
  this->categoria.push_back(categoria);
  cout << endl << "Categoria adicionada!" << endl;
  this->categoria_len++;
}
//==== Muda a quantidade de um produto no estoque ====
void Estoque::mudar_quantidade(){
  string nome_produto;
  int qtd;
  cout << "Digite o nome do produto: ";
  cin.ignore();
  getline(cin,nome_produto);
  for(int i=0;i<this->estoque_len;i++){
    if(nome_produto==this->estoque[i]->get_nome_produto()){
        cout<< "Informe a nova quantidade: ";
        cin >> qtd;
        this->estoque[i]->set_quantidade(qtd);
        cout << endl << "Quantidade atualizada!" << endl;
        return;
    }
  }
  cout << endl << "Produto nao cadastrado." << endl;
}
//==== Diminui a quantidade do produto apos compra ====
void Estoque::mudar_quantidade(string nome_produto,int quantidade){
  for(int i=0;i<this->estoque_len;i++){
    if(nome_produto==this->estoque[i]->get_nome_produto()){
      this->estoque[i]->set_quantidade(estoque[i]->get_quantidade() - quantidade);
      return;
    }
  }
}
//=== Salva o arquivo de categorias ====
void Estoque::save_categorias(){
  if(check_categorias()==true||categoria.size()!=0){
    ofstream categorias;
    categorias.open("archives/categorias.txt",ios::out);
    categorias << this->categoria_len << endl;
    for(int i=0;i<this->categoria_len;i++){
      categorias << this->categoria[i] << endl;
    }
    categorias.close();
  }
}
vector <Produto*> Estoque::vector_estoque(){
  return this->estoque;
}
