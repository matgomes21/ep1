#include "produto.hpp" //Include do hpp
//Includes de bibliotecas necessarias
#include<iostream>
#include<string>

using namespace std;

//==== Metodo construtor generico ====
Produto::Produto(){
    set_nome_produto("");
    set_codigo("");
    set_preco(0);
    set_quantidade(0);
}
//==== Metodo construtor especifico ====
Produto::Produto(string nome_produto, vector <string> categorias, double preco, int quantidade, string codigo){
    set_nome_produto(nome_produto);
    set_categorias(categorias);
    set_preco(preco);
    set_quantidade(quantidade);
    set_codigo(codigo);
    set_categorias_len();
}
//==== Metodo destrutor ====
Produto::~Produto(){
}
//==== Metodos acessores (setters) ====
void Produto::set_nome_produto(string nome_produto){
    this->nome_produto = nome_produto;
}
void Produto::set_categorias(vector <string> categorias){
    this->categorias = categorias;
}
void Produto::set_preco(double preco){
    this->preco = preco;
}
void Produto::set_quantidade(int quantidade){
    this->quantidade = quantidade;
}
void Produto::set_categorias_len(){
    this->categorias_len = int(categorias.size());
}
void Produto::set_codigo(string codigo){
    this->codigo = codigo;
}
//==== Metodos acessores (getters) ====
string Produto::get_nome_produto(){
    return nome_produto;
}
vector <string> Produto::get_vcategorias(){
    return categorias;
}
double Produto::get_preco(){
    return preco;
}
int Produto::get_quantidade(){
    return quantidade;
}
int Produto::get_categorias_len(){
    return categorias_len;
}
string Produto::get_codigo(){
    return codigo;
}
string Produto::get_categorias(int i){
  return this->categorias[i];
}
