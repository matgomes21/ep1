#include "socio.hpp" //Include do hpp
//Includes necessarios
#include <iostream>
#include <string>
#include <fstream>
#include <stdbool.h>

using namespace std;

//==== Metodo construtor generico ====
Socio::Socio(){
    set_nome("Fulano Silva");
    set_cpf("00000000000");
    set_email("example@domain.com");
    set_telefone("(61)999999999");
    set_sexo("I");
    socio_len();
    load_socio();
}
//==== Metodo construtor especifico ====
Socio::Socio(string nome, string cpf, string email, string telefone, string sexo){
    set_nome(nome);
    set_cpf(cpf);
    set_email(email);
    set_telefone(telefone);
    set_sexo(sexo);
}
//==== Metodo destrutor ====
Socio::~Socio(){
}
//==== Metodos Setters ====
void Socio::set_nome(string nome){
    this->nome = nome;
}
void Socio::set_email(string email){
    this->email = email;
}
void Socio::set_telefone(string telefone){
    this->telefone = telefone;
}
void Socio::set_sexo(string sexo){
    this->sexo = sexo;
}
//==== Metodos Getters ====
string Socio::get_nome(){
    return nome;
}
string Socio::get_email(){
    return email;
}
string Socio::get_telefone(){
    return telefone;
}
string Socio::get_sexo(){
    return sexo;
}
//==== Outros metodos ====

//Adiciona os elementos do arquivo socio para um vector
void Socio::load_socio(){
    if(check_socio()==true){
      string aux,nome,cpf,email,telefone,sexo;
      ifstream socios;
      socios.open("archives/socios.txt",ios::in);
      getline(socios,aux);
      Socio* new_socio;
      for(int i=0;i<this->socios_len;i++){
          getline(socios,nome);
          getline(socios,cpf);
          getline(socios,email);
          getline(socios,telefone);
          getline(socios,sexo);
          new_socio = new Socio(nome,cpf,email,telefone,sexo);
          this->socios.push_back(new_socio);
      }
      socios.close();
    }
    else{
      cout << "Não há socios" << endl;
    }
}
//Cadastra novo socio
void Socio::add_socio(){
    string nome,cpf,email,telefone,sexo;
    cout << "Digite o cpf do cliente: ";
    getline(cin,cpf);
    set_cpf(cpf);
    for(int i=0;i<this->socios_len;i++){
      if(cpf==this->socios[i]->get_cpf()){
        cout << endl << "Cliente ja cadastrado" << endl;
        set_nome(this->socios[i]->get_nome());
        set_email(this->socios[i]->get_email());
        set_telefone(this->socios[i]->get_telefone());
        set_sexo(this->socios[i]->get_sexo());
        cout << endl << "Aperte enter para prosseguir..." << endl;
        return;
      }
    }
    cout << "==== Cliente nao cadastrado ====" << endl;
    cout << endl << "Iniciando cadastro..." << endl;
    cout << endl << "Digite o nome completo: ";
    getline(cin,nome);
    cout << endl << "Digite o email: ";
    getline(cin,email);
    cout << endl << "Digite o telefone [Formato: (61)996927595]: ";
    getline(cin,telefone);
    cout << endl << "Digite o sexo ['M' - Masculino/'F' - Feminino/'I' - Prefere nao declarar]: ";
    getline(cin,sexo);
    set_nome(nome);
    set_email(email);
    set_telefone(telefone);
    set_sexo(sexo);

    Socio* new_socio = new Socio(nome,cpf,email,telefone,sexo);

    this->socios.push_back(new_socio);

    cout << endl << "Cliente cadastrado!" << endl;
    cout << endl << "Aperte enter para prosseguir..." << endl;
    this->socios_len++;
}
//Verifica se o arquivo existe
bool Socio::check_socio(){
    ifstream socios;
    socios.open("archives/socios.txt",ios::in);
    if(socios.is_open()){
      socios.close();
      return true;
    }
    else
      return false;
}
//Salva o arquivo socios
void Socio::save_socio(){
    if(check_socio()==true||this->socios.size()!=0){
      ofstream socios;
      socios.open("archives/socios.txt",ios::out);
      socios << this->socios_len << endl;
      for(int i=0;i<this->socios_len;i++){
          socios << this->socios[i]->get_nome() << endl;
          socios << this->socios[i]->get_cpf() << endl;
          socios << this->socios[i]->get_email() << endl;
          socios << this->socios[i]->get_telefone() << endl;
          socios << this->socios[i]->get_sexo() << endl;
      }
      socios.close();
    }
}
//Determina a quantidade de socios
void Socio::socio_len(){
    ifstream socios;
    string socios_len;
    socios.open("archives/socios.txt");
    if(socios.is_open()){
        getline(socios,socios_len);
        this->socios_len = atoi(socios_len.c_str());
        socios.close();
    }
    else
        this->socios_len = 0;
}
//Verifica se o cpf pertence a um socio
bool Socio::check_cpf(){
  string cpf;
  cout << "Informe o cpf do cliente: ";
  getline(cin,cpf);
  for(int i=0;i<this->socios_len;i++){
    if(cpf==this->socios[i]->get_cpf()){
      cout << endl << "Socio ja cadastrado." << endl;
      set_cpf(cpf);
      cout << "Pressione enter para prosseguir..." << endl;
      return true;
    }
  }
  cout << endl << "Socio nao cadastrado." << endl;
  cout << "Pressione enter para prosseguir..." << endl;
  return false;
}
