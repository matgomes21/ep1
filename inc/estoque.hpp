#ifndef ESTOQUE_HPP
#define ESTOQUE_HPP

#include "produto.hpp" //Include de classe necessaria

//Includes de bibliotecas necessarias
#include <vector>
#include <iostream>
#include <stdbool.h>

//==== Criacao da classe estoque ====
class Estoque{
private:
  //Atributos privados
  int estoque_len;
  int categoria_len;
  vector <Produto*> estoque;
  vector <string> categoria;
public:
//==== Metodos Construtor/Destrutor ====
  Estoque();
  ~Estoque();
//==== Metodos Acessores (setters) ====
  void set_estoque_len(); //Determina tamanho do estoque
  void set_categoria_len(); //Determina quantidade de categorias
//==== Metodos Acessores (getters) ====
  int get_estoque_len(); //Pega tamanho do estoque
  int get_categoria_len(); //Pega numero de categorias
  void get_list_categorias(); //Pega a lista de categorias
//==== Outros Metodos ====
  void imprime_estoque(); //Imprime produtos do estoque
  virtual void add_produto(); //Cadastra novo produto ao estoque
  bool check_estoque(); //Verifica se existe o arquivo do estoque
  void save_estoque(); //Salva arquivo do estoque;
  void load_estoque(); //Carrega arquivo do estoque
  bool check_categorias(); //Verifica se existe o arquivo de categorias
  void load_categorias(); //Carrega arquivo de categorias
  void save_categorias(); //Salva o arquivo de categorias
  void add_categoria(); //Adiciona categoria nova
  Produto* search_produto(string nome_produto); //Metodo para buscar produto especifico
  void mudar_quantidade(); //Muda a quantidade de um produto no estoque
  void mudar_quantidade(string nome_produto,int quantidade); //Diminui a quantidade do produto apos compra
  vector <Produto*> vector_estoque(); //Retorna o vector estoque
};
#endif
