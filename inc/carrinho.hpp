#ifndef CARRINHO_HPP
#define CARRINHO_HPP
#include "estoque.hpp" //Include da classe mae
//Includes de bibliotecas necessarias
#include <string>
#include <iostream>
#include <vector>
#include <stdbool.h>

using namespace std;
//Criacao da classe carrinho, filha da classe estoque
class Carrinho: public Estoque{
private: //Atributos privados
    double preco_total;
    vector <Produto*> carrinho;
    int carrinho_len;
public:
//==== Construtor e Destrutor ====
    Carrinho();
    ~Carrinho();
//==== Setters ====
    void set_preco_total(); //Determina preco total
    void set_carrinho_len(); //Determina tamanho do carrinho
//==== Getters ====
    double get_preco_total(); //Retorna preco total
    int get_carrinho_len(); //Retorna tamanho do carrinho
    vector <Produto*> get_carrinho(); //Recebe carrinho em vector
//==== Outros Metodos ====
    void imprime_carrinho(); //Imprime produtos do carrinho
    void add_produto(); //Adiciona produto ao carrinho
    void remove_produto(); //Remove produto do carrinho
    bool finalizar_compra(); //Finaliza a compra
};
#endif
