#ifndef RECOMENDACAO_HPP
#define RECOMENDACAO_HPP

#include "produto.hpp" //Include de hpp necessario
//Includes de bibliotecas necessarias
#include <iostream>
#include <vector>
#include <string>
#include <stdbool.h>

class Recomendacao{
private: //Atributos privados
  int rcategorias_len;
  int historico_len;
  string nome;
  vector <int> socio;
  vector <string> categorias;
  vector <Produto*> estoque;
  vector <Produto*> historico;
public:
  //==== Construtores e Destrutor ====
  Recomendacao();
  Recomendacao(string cpf);
  Recomendacao(vector <Produto*> historico);
  ~Recomendacao();
  //==== Setters ====
  void set_rcategorias_len();
  void set_historico_len();

  //==== Outros Metodos ====
  void concat(string cpf); //
  void load_historico(vector <Produto*> historico); //
  void save_recomendacao(); //
  void load_recomendacao(); //
  bool check_recomendacao(); //
  void sort(); //
  void carrega_estoque(vector <Produto*> estoque); //
  void define_categorias(); //
  void recomendacao(); //
};
#endif
