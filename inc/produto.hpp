#ifndef PRODUTO_HPP
#define PRODUTO_HPP
//Includes de bibliotecas necessarias
#include <string>
#include <vector>
#include <iostream>

using namespace std;

class Produto{
private: //Atributos privados
    int quantidade; //Quantidade de produtos
    int categorias_len;
    string nome_produto;
    string codigo;
    double preco; //Em reais
    vector <string> categorias;
public:
//==== Metodos Construtores/Destrutor ====
    Produto();
    Produto(string nome_produto, vector <string> categoria, double preco, int quantidade, string codigo);
    ~Produto();
//==== Metodos acessores (setters) ====
    void set_nome_produto(string nome_produto);
    void set_categorias(vector <string> categorias);
    void set_preco(double preco);
    void set_quantidade(int quantidade);
    void set_categorias_len();
    void set_codigo(string codigo);
//==== Metodos acessores (getters) ====
    string get_nome_produto();
    vector <string> get_vcategorias();
    double get_preco();
    int get_quantidade();
    int get_categorias_len();
    string get_codigo();
    string get_categorias(int i);

};
#endif
