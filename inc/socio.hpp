#ifndef SOCIO_HPP
#define SOCIO_HPP
#include "cliente.hpp" //Include do hpp da classe mae
//Includes de bibliotecas necessarias
#include <string>
#include <vector>

using namespace std;

//Classe Socio, filha da classe Cliente
class Socio:public Cliente{
private:
//==== Atributos Privados ====
    string nome; //Formato: "Nome Sobrenome"
    string email; //Formato: "example@domain.com"
    string telefone; //Formato: "(61)999999999"
    string sexo; //Opcoes: 'M' - masculino, 'F' - feminino, 'I' - prefere nao declarar
    vector <Socio*> socios; //Vector para armazenar os socios
    int socios_len; //Tamanho do vector socios

public:
//==== Metodos construtores/destrutor ====
    Socio();
    Socio(string nome, string cpf, string email, string telefone, string sexo);
    ~Socio();
//==== Metodos acessores (setters) ====
    void set_nome(string nome);
    void set_email(string email);
    void set_telefone(string telefone);
    void set_sexo(string sexo);
//==== Metodos acessores (getters) ====
    string get_nome();
    string get_email();
    string get_telefone();
    string get_sexo();
//==== Outros metodos ====
    void load_socio(); //Adiciona os elementos do arquivo socios para um vector
    void add_socio(); //Cadastra novo socio
    bool check_socio(); //Verifica se o arquivo socios existe
    void save_socio(); //Salva o arquivo socios
    void socio_len(); //Determina a quantidade de socios
    bool check_cpf(); //Verifica se o cpf pertence a um socio
};
#endif
