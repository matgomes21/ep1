#ifndef CLIENTE_HPP
#define CLIENTE_HPP
#include <string> //Include de biblioteca necessaria

using namespace std;

class Cliente{
private: //Atributo privado
  string cpf;
public:
//Metodos Construtor/Destrutor
  Cliente();
  ~Cliente();
//Metodo Acessor (set)
  void set_cpf(string cpf);
//Metodo Acessor (get)
  string get_cpf();
};
#endif
